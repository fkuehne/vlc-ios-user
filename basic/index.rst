.. _basic:

###################
  Basic Usage
###################

.. toctree::
   :maxdepth: 3
   :includehidden:
   :name: toc-basic

   mediagroups.rst
   audio.rst
   playlist.rst
   playbackgestures.rst
   settings.rst