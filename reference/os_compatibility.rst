.. _os_compatibility:

.. raw:: html

    <style> .red {color:#DC143C} </style>
    <style> .green {color:#32CD32} </style>
    <style> .blue {color:#8A2BE2} </style>

.. role:: red
.. role:: green
.. role:: blue


OS Compatibility Matrix
#######################

Version Compatibility Summary


+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|                 |          |             |             |             |             |                         |
|  VLC Version    |   1.x    |2.0.x 2.1.x  | 2.2.x 2.3.x |  3.0.x      | 3.1.x       |      *Remarks*          |
|                 |          |             |             |             | 3.2.x       |                         |
+=================+==========+=============+=============+=============+=============+=========================+
|   **6.1**       |:red:`NO` |:green:`YES` |:green:`YES` |:green:`YES` |:green:`YES` |  Older Release          |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|   **7.0**       |:red:`NO` |:green:`YES` |:green:`YES` |:green:`YES` |:green:`YES` |  Older Release          |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|   **8.0**       |:red:`NO` |:green:`YES` |:green:`YES` |:green:`YES` |:green:`YES` |  Older Release          |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|**9.0.x-9.3.x**  |:red:`NO` |:red:`NO`    |:red:`NO`    |:green:`YES` |:green:`YES` |                         |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|**10.0.x-10.3.x**|:red:`NO` |:red:`NO`    |:red:`NO`    |:green:`YES` |:green:`YES` |                         |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|**11.0.x-11.4.x**|:red:`NO` |:red:`NO`    |:red:`NO`    |:green:`YES` |:green:`YES` |                         |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|**12.0.x-12.4.x**|:red:`NO` |:red:`NO`    |:red:`NO`    |:green:`YES` |:green:`YES` |                         |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+
|**13.0-13.5.x**  |:red:`NO` |:red:`NO`    |:red:`NO`    |:green:`YES` |:green:`YES` |                         |
+-----------------+----------+-------------+-------------+-------------+-------------+-------------------------+

.. warning::
    iOS versions 6.1, 7.0, 8.0 are deprecated. Upgrading to a more recent iOS version is strongly recommended.