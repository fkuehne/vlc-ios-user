.. _getting_started:

###################
  Getting Started
###################

.. toctree::
   :maxdepth: 3
   :includehidden:
   
   setup.rst
   media_synchronization.rst