.. _setup:

#####
Setup
#####

This section covers the steps needed to download and set up VLC on an iOS device.

1. Open your iOS device and navigate to the **App Store**.

.. figure::  /images/gettingstarted/setup/playstore.jpeg
   :align:   center
   :width: 40%

2. On the ***App Store**, search for :guilabel:`VLC for Mobile`.

.. figure::  /images/gettingstarted/setup/search.jpeg
   :align:   center
   :width: 40%

3. On the :guilabel:`VLC for Mobile` pane, tap on :guilabel:`GET`.

.. figure::  /images/gettingstarted/setup/download_vlc.png
   :align:   center
   :width: 40%

4. Depending on the version of your iOS device, you will be required to authenticate the download using either your Apple ID, fingerprint, or Face ID.

.. figure::  /images/gettingstarted/setup/approve_download.png
   :align:   center
   :width: 40%

5. After you've authenticated the download, the VLC for iOS application will be downloaded into your device successfully.

.. figure::  /images/gettingstarted/setup/vlc_icon.jpeg
   :align:   center
   :width: 40%

6. Open your VLC for iOS application, and **add** or **watch** media files using any of our :doc:`/gettingstarted/media_synchronization` methods.