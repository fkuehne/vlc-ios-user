.. _glossary:

############
  Glossary
############

This page lists definitions for terms used in VLC and this documentation.

.. glossary::
   :sorted: